from django.contrib import admin

from greg.models import (
    Person,
    PersonRole,
    Role,
)


class PersonAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "role_count",
    )
    search_fields = ("first_name", "last_name")  # TODO: "identities__value"?
    readonly_fields = ("id", "created", "updated")

    def role_count(self, person):
        return str(person.roles.count())

    role_count.short_description = "# roles"  # type: ignore


class PersonRoleAdmin(admin.ModelAdmin):
    list_display = ("id", "person", "role")
    search_fields = (
        "person__id",
        "role__id",
    )
    raw_id_fields = ("person", "role")
    readonly_fields = ("id", "created", "updated")


class RoleAdmin(admin.ModelAdmin):
    list_display = ("id", "type", "name_nb", "name_en")
    readonly_fields = ("id", "created", "updated")


admin.site.register(Person, PersonAdmin)
admin.site.register(PersonRole, PersonRoleAdmin)
admin.site.register(Role, RoleAdmin)
