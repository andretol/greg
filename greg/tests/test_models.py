from django.db.models import ProtectedError
from django.test import TestCase

from greg.models import (
    Person,
    Role,
    PersonRole,
    OrganizationalUnit,
    Sponsor,
    SponsorOrganizationalUnit,
    Consent,
)


class PersonModelTests(TestCase):
    test_person = dict(
        first_name="Test",
        last_name="Tester",
        date_of_birth="2000-01-27",
        email="test@example.org",
        mobile_phone="123456789",
    )

    def test_add_multiple_roles_to_person(self):
        role1 = Role.objects.create(type="role1", name_en="Role 1")
        role2 = Role.objects.create(type="role2", name_en="Role 2")

        unit = OrganizationalUnit.objects.create(orgreg_id="12345", name_en="Test unit")
        sponsor = Sponsor.objects.create(feide_id="test@uio.no")
        sponsor2 = Sponsor.objects.create(feide_id="test2@uio.no")

        person = Person.objects.create(**self.test_person)

        # Add two roles to the person and check that they appear when listing the roles for the person
        PersonRole.objects.create(
            person=person,
            role=role1,
            unit=unit,
            start_date="2020-03-05",
            end_date="2020-06-10",
            contact_person_unit="Contact Person",
            available_in_search=True,
            registered_by=sponsor,
        )

        PersonRole.objects.create(
            person=person,
            role=role2,
            unit=unit,
            start_date="2021-03-05",
            end_date="2021-06-10",
            contact_person_unit="Contact Person",
            available_in_search=True,
            registered_by=sponsor2,
        )

        person_roles = person.roles.all()

        self.assertEqual(2, len(person_roles))
        self.assertIn(role1, person_roles)
        self.assertIn(role2, person_roles)

    def test_person_not_allowed_deleted_when_roles_are_present(self):
        person = Person.objects.create(**self.test_person)
        role = Role.objects.create(type="role1", name_en="Role 1")
        unit = OrganizationalUnit.objects.create(orgreg_id="12345", name_en="Test unit")
        sponsor = Sponsor.objects.create(feide_id="test@uio.no")

        PersonRole.objects.create(
            person=person,
            role=role,
            unit=unit,
            start_date="2020-03-05",
            end_date="2020-06-10",
            contact_person_unit="Contact Person",
            available_in_search=True,
            registered_by=sponsor,
        )

        # It is not clear what cleanup needs to be done when a person is going to be
        # removed, so for now is prohibited to delete a person if there is data
        # attached to him in other tables
        self.assertRaises(ProtectedError, person.delete)


class SponsorModelTests(TestCase):
    def test_add_sponsor_to_multiple_units(self):
        sponsor = Sponsor.objects.create(feide_id="test@uio.no")

        unit1 = OrganizationalUnit.objects.create(
            orgreg_id="12345", name_en="Test unit"
        )
        unit2 = OrganizationalUnit.objects.create(
            orgreg_id="123456", name_en="Test unit2"
        )

        SponsorOrganizationalUnit.objects.create(
            sponsor=sponsor, organizational_unit=unit1, hierarchical_access=False
        )
        SponsorOrganizationalUnit.objects.create(
            sponsor=sponsor, organizational_unit=unit2, hierarchical_access=False
        )

        sponsor_units = sponsor.units.all()

        self.assertEqual(2, len(sponsor_units))
        self.assertIn(unit1, sponsor_units)
        self.assertIn(unit2, sponsor_units)

    def test_add_multiple_sponsors_to_unit(self):
        sponsor1 = Sponsor.objects.create(feide_id="test@uio.no")
        sponsor2 = Sponsor.objects.create(feide_id="test2@uio.no")

        unit = OrganizationalUnit.objects.create(orgreg_id="12345", name_en="Test unit")
        unit2 = OrganizationalUnit.objects.create(
            orgreg_id="123456", name_en="Test unit2"
        )

        SponsorOrganizationalUnit.objects.create(
            sponsor=sponsor1, organizational_unit=unit, hierarchical_access=False
        )
        SponsorOrganizationalUnit.objects.create(
            sponsor=sponsor2, organizational_unit=unit, hierarchical_access=True
        )

        sponsor1_units = sponsor1.units.all()
        self.assertEqual(1, len(sponsor1_units))
        self.assertIn(unit, sponsor1_units)

        sponsor2_units = sponsor2.units.all()
        self.assertEqual(1, len(sponsor2_units))
        self.assertIn(unit, sponsor2_units)

        sponsors_for_unit = Sponsor.objects.filter(units=unit.id)
        self.assertEqual(2, len(sponsors_for_unit))
        self.assertIn(sponsor1, sponsors_for_unit)
        self.assertIn(sponsor2, sponsors_for_unit)

        sponsors_for_unit2 = Sponsor.objects.filter(units=unit2.id)
        self.assertEqual(0, len(sponsors_for_unit2))


class ConsentModelTest(TestCase):
    def test_add_consent_to_person(self):
        person = Person.objects.create(
            first_name="Test",
            last_name="Tester",
            date_of_birth="2000-01-27",
            email="test@example.org",
            mobile_phone="123456789",
        )

        consent = Consent.objects.create(
            type="it_guidelines",
            consent_name_en="IT Guidelines",
            consent_name_nb="IT Regelverk",
            consent_description_en="IT Guidelines description",
            consent_description_nb="IT Regelverk beskrivelse",
            consent_link_en="https://example.org/it_guidelines",
            user_allowed_to_change=False,
        )

        person.consents.add(
            consent, through_defaults={"consent_given_at": "2021-06-20"}
        )


class OrganizationalUnitTest(TestCase):
    def test_set_parent_for_unit(self):
        parent = OrganizationalUnit.objects.create(
            orgreg_id="12345", name_en="Parent unit", name_nb="Foreldreseksjon"
        )
        child = OrganizationalUnit.objects.create(
            orgreg_id="123456",
            name_en="Child unit",
            name_nb="Barneseksjon",
            parent=parent,
        )

        query_result = OrganizationalUnit.objects.filter(parent__id=parent.id)
        self.assertEqual(1, len(query_result))
        self.assertIn(child, query_result)
