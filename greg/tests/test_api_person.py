from django.contrib.auth import get_user_model
from django.utils.dateparse import parse_datetime
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient, APITestCase
from rest_framework.authtoken.models import Token

from django.test import TestCase

from greg.models import Person, PersonRole, Role


class GregAPITestCase(APITestCase):
    def setUp(self):
        self.client = self.get_token_client()

    def get_token_client(self, username="test") -> APIClient:
        self.user, created = get_user_model().objects.get_or_create(username=username)
        token, created = Token.objects.get_or_create(user=self.user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token {}".format(token.key))
        return client


class PersonTestData:
    def setUp(self):
        super().setUp()
        self.person_foo_data = dict(
            first_name="Foo",
            last_name="Foo",
            date_of_birth="2000-01-27",
            email="test@example.org",
            mobile_phone="123456788",
        )
        self.person_bar_data = dict(
            first_name="Bar",
            last_name="Bar",
            date_of_birth="2000-07-01",
            email="test2@example.org",
            mobile_phone="123456789",
        )
        self.person_foo = Person.objects.create(**self.person_foo_data)
        self.person_bar = Person.objects.create(**self.person_bar_data)


class PersonAPITestCase(PersonTestData, GregAPITestCase):
    def test_get_person(self):
        url = reverse("person-detail", kwargs={"id": self.person_foo.id})
        response = self.client.get(url)
        data = response.json()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get("id"), self.person_foo.id)
        self.assertEqual(data.get("first_name"), self.person_foo.first_name)
        self.assertEqual(data.get("last_name"), self.person_foo.last_name)

    def test_persons(self):
        url = reverse("person-list")
        response = self.client.get(url)
        data = response.json()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, len(data["results"]))
