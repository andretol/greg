from .notification import NotificationSerializer
from .person import PersonSerializer
from .role import RoleSerializer


def get_serializer(instance):
    return {
        "notification": NotificationSerializer,
        "person": PersonSerializer,
        "role": RoleSerializer,
    }.get(instance._meta.verbose_name)
