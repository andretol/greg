from rest_framework import serializers

from greg.models import Person, PersonRole, Role


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ("id", "first_name", "last_name")


class PersonRoleSerializer(serializers.ModelSerializer):
    role = serializers.SlugRelatedField(queryset=Role.objects.all(), slug_field="type")

    class Meta:
        model = PersonRole
        fields = [
            "id",
            "created",
            "updated",
            "role",
        ]
