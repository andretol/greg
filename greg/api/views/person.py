from django_filters import rest_framework as filters

from rest_framework import viewsets

from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.person import PersonSerializer, PersonRoleSerializer
from greg.api.filters import PersonFilter, PersonRoleFilter
from greg.models import Person, PersonRole


class PersonViewSet(viewsets.ModelViewSet):
    """Person API"""

    queryset = Person.objects.all().order_by("id")
    serializer_class = PersonSerializer
    pagination_class = PrimaryKeyCursorPagination
    lookup_field = "id"
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PersonFilter


class PersonRoleViewSet(viewsets.ModelViewSet):
    """Person role API"""

    queryset = PersonRole.objects.all().order_by("id")
    serializer_class = PersonRoleSerializer
    pagination_class = PrimaryKeyCursorPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PersonRoleFilter

    def get_queryset(self):
        qs = self.queryset
        if not self.kwargs:
            return qs.none()
        person_id = self.kwargs["person_id"]
        person_role_id = self.kwargs.get("id")
        qs = qs.filter(person_id=person_id)
        if person_role_id:
            qs = qs.filter(id=person_role_id)
        return qs
