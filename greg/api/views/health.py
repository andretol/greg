from django.http import HttpResponse
from drf_spectacular.utils import extend_schema
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView


class Health(APIView):
    """Health checks."""

    authentication_classes = []
    permission_classes = []

    @extend_schema(
        responses={200: str},
    )
    def get(self, request, *args, **kwargs):
        """Always gives a thumbs up by returning `OK` as plain text."""
        return HttpResponse("OK", content_type="text/plain", status=HTTP_200_OK)
