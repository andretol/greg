from django_filters import rest_framework as filters

from rest_framework import viewsets
from rest_framework.schemas.openapi import AutoSchema

from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.role import RoleSerializer
from greg.models import Role


class RoleViewSet(viewsets.ModelViewSet):
    """Role API"""

    queryset = Role.objects.all().order_by("id")
    serializer_class = RoleSerializer
    pagination_class = PrimaryKeyCursorPagination
    lookup_field = "id"
