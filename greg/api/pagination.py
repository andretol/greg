from rest_framework.pagination import CursorPagination


class PrimaryKeyCursorPagination(CursorPagination):
    ordering = "pk"
    page_size = 100
