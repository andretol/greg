from django_filters import rest_framework as filters

from greg.models import Person, PersonRole


class PersonRoleFilter(filters.FilterSet):
    type = filters.BaseInFilter(field_name="role__type", lookup_expr="in")

    class Meta:
        model = PersonRole
        fields = ["type"]


class PersonFilter(filters.FilterSet):
    class Meta:
        model = Person
        fields = ["first_name"]
