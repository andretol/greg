from django.conf.urls import url
from django.urls import path
from rest_framework.routers import DefaultRouter
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from greg.api.views.person import PersonViewSet, PersonRoleViewSet
from greg.api.views.role import RoleViewSet
from greg.api.views.health import Health

router = DefaultRouter()
router.register(r"persons", PersonViewSet, basename="person")
router.register(r"roles", RoleViewSet, basename="role")

urlpatterns = router.urls

urlpatterns += [
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path("health/", Health.as_view()),
    url(
        r"^persons/(?P<person_id>[0-9]+)/roles/$",
        PersonRoleViewSet.as_view({"get": "list"}),
        name="person_role-list",
    ),
    url(
        r"^persons/(?P<person_id>[0-9]+)/roles/(?P<id>[0-9]+)/$",
        PersonRoleViewSet.as_view({"get": "retrieve"}),
        name="person_role-detail",
    ),
]
