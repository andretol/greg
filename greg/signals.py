import time
import logging

from django.db import models
from django.dispatch import receiver

from greg.models import (
    Person,
    PersonRole,
    Role,
    Notification,
)

logger = logging.getLogger(__name__)

SUPPORTED_MODELS = (
    Person,
    PersonRole,
    Role,
)


@receiver(models.signals.pre_migrate)
def disconnect_notification_signals(*args, **kwargs):
    """
    Disconnect signals before migration.
    This ensures that we can migrate,
    as the signals are called when table is saved.
    """
    models.signals.pre_save.disconnect(dispatch_uid="add_changed_fields_callback")
    models.signals.post_save.disconnect(dispatch_uid="save_notification_callback")
    models.signals.post_delete.disconnect(dispatch_uid="delete_notification_callback")


@receiver(models.signals.post_migrate)
def connect_notification_signals(*args, **kwargs):
    """
    Add back signals after migration.
    """
    models.signals.pre_save.connect(
        receiver=add_changed_fields_callback,
        dispatch_uid="add_changed_fields_callback",
    )
    models.signals.post_save.connect(
        receiver=save_notification_callback,
        dispatch_uid="save_notification_callback",
    )
    models.signals.post_delete.connect(
        receiver=delete_notification_callback,
        dispatch_uid="delete_notification_callback",
    )


def _store_notification(identifier, object_type, operation, **kwargs):
    # logger.debug(
    #     '_store_notification identifier=%r, object_type=%r, operation=%r, kwargs=%r',
    #     identifier, object_type, operation, kwargs)
    return Notification.objects.create(
        identifier=identifier,
        object_type=object_type,
        operation=operation,
        issued_at=int(time.time()),
        meta=kwargs,
    )


@receiver(models.signals.pre_save, dispatch_uid="add_changed_fields_callback")
def add_changed_fields_callback(sender, instance, raw, *args, **kwargs):
    """
    Makes note of any dirty (changed) fields before they are saved,
    stuffing them in the instance for use in any post-save callbacks.
    """
    if not isinstance(instance, (Person, PersonRole)):
        return
    changed = instance.is_dirty()
    if not changed:
        return
    instance._changed_fields = list(instance.get_dirty_fields().keys())


@receiver(models.signals.post_save, dispatch_uid="save_notification_callback")
def save_notification_callback(sender, instance, created, *args, **kwargs):
    if not isinstance(instance, SUPPORTED_MODELS):
        return
    meta = {}
    operation = "add" if created else "update"
    if isinstance(instance, PersonRole):
        meta["person_id"] = instance.person.id
        meta["role_id"] = instance.role.id
    _store_notification(
        identifier=instance.id,
        object_type=instance._meta.object_name,
        operation=operation,
        **meta
    )


@receiver(models.signals.post_delete, dispatch_uid="delete_notification_callback")
def delete_notification_callback(sender, instance, *args, **kwargs):
    if not isinstance(instance, SUPPORTED_MODELS):
        return
    meta = {}
    if isinstance(instance, PersonRole):
        meta["person_id"] = instance.person.id
        meta["role_id"] = instance.role.id
    _store_notification(
        identifier=instance.id,
        object_type=instance._meta.object_name,
        operation="delete",
        **meta
    )
