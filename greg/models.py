from datetime import date

from dirtyfields import DirtyFieldsMixin
from django.db import models
from django.db.models import Lookup
from django.db.models.fields import Field


@Field.register_lookup
class Like(Lookup):
    """Allows doing LIKE comparisons when querying."""

    lookup_name = "like"

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return "%s LIKE %s" % (lhs, rhs), params


class BaseModel(DirtyFieldsMixin, models.Model):
    """Common fields for all models."""

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Person(BaseModel):
    """A person is someone who has requested guest access."""

    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    date_of_birth = models.DateField()
    email = models.EmailField()
    email_verified_date = models.DateField(null=True, blank=True)
    mobile_phone = models.CharField(max_length=15)
    mobile_phone_verified_date = models.DateField(null=True, blank=True)
    registration_completed_date = models.DateField(null=True, blank=True)
    token = models.CharField(max_length=32, blank=True)
    roles = models.ManyToManyField("Role", through="PersonRole", related_name="persons")
    consents = models.ManyToManyField(
        "Consent", through="PersonConsent", related_name="consent"
    )

    def __str__(self):
        return "{} {} ({})".format(self.first_name, self.last_name, self.pk)

    def __repr__(self):
        return "{}(id={!r}, first_name={!r}, last_name={!r})".format(
            self.__class__.__name__,
            self.pk,
            self.first_name,
            self.last_name,
        )


class Role(BaseModel):
    """A role variant."""

    type = models.SlugField(max_length=64, unique=True)
    name_nb = models.CharField(max_length=256)
    name_en = models.CharField(max_length=256)
    description_nb = models.TextField()
    description_en = models.TextField()
    default_duration_days = models.IntegerField(null=True)

    def __str__(self):
        return str(self.name_nb or self.name_en or self.slug)

    def __repr__(self):
        return "{}(id={!r}, type={!r}, name_nb={!r}, name_en={!r})".format(
            self.__class__.__name__,
            self.pk,
            self.type,
            self.name_nb,
            self.name_en,
        )


class PersonRole(BaseModel):
    """The relationship between a person and a role."""

    person = models.ForeignKey(
        "Person", on_delete=models.PROTECT, related_name="person_roles"
    )
    role = models.ForeignKey(
        "Role", on_delete=models.PROTECT, related_name="person_roles"
    )
    unit = models.ForeignKey(
        "OrganizationalUnit", on_delete=models.PROTECT, related_name="unit_person_role"
    )
    start_date = models.DateField()
    end_date = models.DateField()
    # TODO Is this field needed?
    contact_person_unit = models.TextField()
    comments = models.TextField(blank=True)
    available_in_search = models.BooleanField(default=False)
    registered_by = models.ForeignKey(
        "Sponsor", on_delete=models.PROTECT, related_name="sponsor_role"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["person", "role"], name="personrole_person_role_unique"
            )
        ]

    def __repr__(self):
        return "{}(id={!r}, person={!r}, role={!r})".format(
            self.__class__.__name__, self.pk, self.person, self.role
        )


class Notification(BaseModel):
    """A change notification that should be delivered to a message queue."""

    identifier = models.IntegerField(null=True, blank=True)
    object_type = models.TextField()
    operation = models.TextField()
    issued_at = models.IntegerField()
    meta = models.JSONField(null=True, blank=True)

    def __repr__(self):
        return "{}(id={!r}, identifier={!r}, object_type={!r}, operation={!r}, issued_at={!r}), meta={!r})".format(
            self.__class__.__name__,
            self.pk,
            self.identifier,
            self.object_type,
            self.operation,
            self.issued_at,
            self.meta,
        )


class PersonIdentity(BaseModel):
    # TODO: Add more types
    class IdentityType(models.TextChoices):
        PASSPORT_NUMBER = "PASSPORT_NUMBER"
        FEIDE_ID = "FEIDE_ID"

    class Verified(models.TextChoices):
        AUTOMATIC = "AUTOMATIC"
        MANUAL = "MANUAL"

    person = models.ForeignKey(
        "Person", on_delete=models.PROTECT, related_name="person"
    )
    type = models.CharField(max_length=15, choices=IdentityType.choices)
    source = models.CharField(max_length=256)
    value = models.CharField(max_length=256)
    verified = models.CharField(max_length=9, choices=Verified.choices, blank=True)
    verified_by = models.ForeignKey(
        "Sponsor", on_delete=models.PROTECT, related_name="sponsor", null=True
    )
    verified_when = models.DateField(blank=True)

    def __repr__(self):
        return (
            "{}(id={!r}, type={!r}, source={!r}, value={!r}, verified_by={!r})".format(
                self.__class__.__name__,
                self.pk,
                self.type,
                self.source,
                self.value,
                self.verified_by,
            )
        )


class Consent(BaseModel):
    """
    Describes some consent, like acknowledging the IT department guidelines, a guest can give.
    """

    type = models.SlugField(max_length=64, unique=True)
    consent_name_en = models.CharField(max_length=256)
    consent_name_nb = models.CharField(max_length=256)
    consent_description_en = models.TextField()
    consent_description_nb = models.TextField()
    consent_link_en = models.URLField(null=True)
    consent_link_nb = models.URLField(null=True)
    valid_from = models.DateField(default=date.today)
    user_allowed_to_change = models.BooleanField()

    def __repr__(self):
        return "{}(id={!r}, type={!r}, consent_name_en={!r}, valid_from={!r}, user_allowed_to_change={!r})".format(
            self.__class__.__name__,
            self.pk,
            self.type,
            self.consent_name_en,
            self.valid_from,
            self.user_allowed_to_change,
        )


class PersonConsent(BaseModel):
    """
    Links a person and a consent he has given.
    """

    person = models.ForeignKey(
        "Person", on_delete=models.PROTECT, related_name="link_person_consent"
    )
    consent = models.ForeignKey(
        "Consent", on_delete=models.PROTECT, related_name="link_person_consent"
    )
    consent_given_at = models.DateField()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["person", "consent"], name="person_consent_unique"
            )
        ]

    def __repr__(self):
        return "{}(id={!r}, person={!r}, consent={!r}, consent_given_at={!r})".format(
            self.__class__.__name__,
            self.pk,
            self.person,
            self.consent,
            self.consent_given_at,
        )


class OrganizationalUnit(BaseModel):
    """
    An organizational unit. Units can be organized in a hierarchical manner.
    """

    orgreg_id = models.CharField(max_length=256)
    name_nb = models.CharField(max_length=256)
    name_en = models.CharField(max_length=256)
    parent = models.ForeignKey("self", on_delete=models.PROTECT, null=True)

    def __repr__(self):
        return "{}(id={!r}, orgreg_id={!r}, name_en={!r}, parent={!r})".format(
            self.__class__.__name__, self.pk, self.orgreg_id, self.name_en, self.parent
        )

    class Meta:
        constraints = [
            models.UniqueConstraint(name="unique_orgreg_id", fields=["orgreg_id"])
        ]


class Sponsor(BaseModel):
    """
    A sponsor is someone who is allowed, with some restrictions, to send out invitations to guests and to verify their identity.
    """

    feide_id = models.CharField(max_length=256)
    units = models.ManyToManyField(
        "OrganizationalUnit",
        through="SponsorOrganizationalUnit",
        related_name="sponsor_unit",
    )

    def __repr__(self):
        return "{}(id={!r}, feide_id={!r})".format(
            self.__class__.__name__, self.pk, self.feide_id
        )

    class Meta:
        constraints = [
            models.UniqueConstraint(name="unique_feide_id", fields=["feide_id"])
        ]


class SponsorOrganizationalUnit(BaseModel):
    """
    A link between a sponsor and an organizational unit.
    """

    sponsor = models.ForeignKey(
        "Sponsor", on_delete=models.PROTECT, related_name="link_sponsor"
    )
    organizational_unit = models.ForeignKey(
        "OrganizationalUnit", on_delete=models.PROTECT, related_name="link_unit"
    )
    hierarchical_access = models.BooleanField()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["sponsor", "organizational_unit"],
                name="sponsor_organizational_unit_unique",
            )
        ]

    def __repr__(self):
        return "{}(id={!r}, sponsor={!r}, organizational_unit={!r}, hierarchical_access={!r})".format(
            self.__class__.__name__,
            self.pk,
            self.sponsor,
            self.organizational_unit,
            self.hierarchical_access,
        )
