from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class GregAppConfig(AppConfig):
    name = "greg"
    verbose_name = _("Greg")

    def ready(self):
        import greg.signals
