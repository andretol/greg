import argparse
import datetime
import signal
import os

from django.core.management.base import BaseCommand, CommandError

from django.conf import settings


class Command(BaseCommand):
    help = "Stop notification publisher"

    def handle(self, *args, **options):
        try:
            with open(settings.NOTIFICATION_PUBLISHER["daemon"]["pid_file"], "r") as f:
                pid = int(f.read().strip())

            os.kill(pid, signal.SIGINT)
        except FileNotFoundError:
            pass
