import logging
import sys
import json
import signal

import daemon
from daemon import pidfile
import lockfile
from django.core.management.base import BaseCommand, CommandError

from greg.models import Notification
from greg.api.serializers import get_serializer
from pika_context_manager import PCM

from django.conf import settings

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger()


def exception_handler(ex_cls, ex, tb):
    logger.critical("Uncaught exception:", exc_info=(ex_cls, ex, tb))


sys.excepthook = exception_handler


def generate_routing_key(system_name, n):
    return "{system_name}.{object_type}.{operation}".format(
        system_name=system_name, **n
    )


def get_notifications(limit):
    return Notification.objects.all()[:limit]


def handle_one_notification(notification: Notification, pcm: PCM, exchange: str):
    serializer = get_serializer(notification)
    representation = serializer().to_representation(notification)
    payload = json.dumps(representation)
    routing_key = generate_routing_key("greg", representation)

    if pcm.publish(
        exchange,
        routing_key,
        payload,
        content_type="application/json",
    ):
        logger.info(
            "Published message %s with routing key %s to exchange %s",
            payload,
            routing_key,
            exchange,
        )

        notification.delete()
    else:
        logger.error(
            "Failed published message %s with routing key %s to exchange %s",
            payload,
            routing_key,
            exchange,
        )


class RunState:
    def __init__(self):
        self.run = True

    def stop(self, *args):
        self.run = False


class Command(BaseCommand):
    help = "Publish notifications via AMQP"

    def add_arguments(self, parser):
        parser.add_argument(
            "--detach", action="store_true", help="Run detached as a dæmon"
        )
        parser.add_argument(
            "--use-pidfile", action="store_true", help="Use a PID lockfile"
        )

    def handle(self, *args, **options):
        state = RunState()
        lock_context = None
        if options.get("use_pidfile"):
            lock_context = pidfile.TimeoutPIDLockFile(
                settings.NOTIFICATION_PUBLISHER["daemon"]["pid_file"]
            )

        try:
            with daemon.DaemonContext(
                pidfile=lock_context,
                files_preserve=[x.stream.fileno() for x in logger.handlers],
                stderr=sys.stderr,
                stdout=sys.stdout,
                signal_map={signal.SIGINT: state.stop, signal.SIGTERM: state.stop},
                detach_process=options.get("detach"),
            ):
                logger.info(
                    "Running %s", "detached" if options.get("detach") else "attached"
                )
                with PCM(**settings.NOTIFICATION_PUBLISHER["mq"]["connection"]) as pcm:
                    exchange = settings.NOTIFICATION_PUBLISHER["mq"]["exchange"]
                    while state.run:
                        for notification in get_notifications(limit=500):
                            handle_one_notification(
                                notification=notification, pcm=pcm, exchange=exchange
                            )

                        pcm.sleep(
                            settings.NOTIFICATION_PUBLISHER["daemon"]["poll_interval"]
                        )
        except lockfile.AlreadyLocked as e:
            logger.warning("Can't start daemon: %s", e)
            # TODO: Figure out how to emit a code other than 0 here
            # TODO: Also, echo the error to stderr
        finally:
            logger.info("Stopped")
