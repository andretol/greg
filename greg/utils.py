import datetime
import re


def camel_to_snake(s: str) -> str:
    """Turns `FooBar` into `foo_bar`."""
    return re.sub("([A-Z])", "_\\1", s).lower().lstrip("_")


def utcnow() -> datetime.datetime:
    """The current date and time with the timezone set to UTC."""
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
