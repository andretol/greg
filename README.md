# Greg

Greg (Guest REGistration) provides a core API for keeping track of guests at an institution.

It is built on [Django](https://www.djangoproject.com/) and [REST framework](https://www.django-rest-framework.org/).

## Installation

[Poetry](https://python-poetry.org/docs/) is used for dependency management.

    # Make sure poetry is installed, see docs
    poetry shell        # Start a shell, creating a virtual environment.
    poetry install      # Install dependencies from lock file

## Configuration

Local configuration is read from `gregsite/settings/local.py`. Put secrets and local modifications here.

    # Make sure a local settings file exists
    $ touch gregsite/settings/local.py

`gregsite/settings/base.py` contains common configuration for all environments. Put defaults here.\
`gregsite/settings/dev.py` contains development configuration. It's used when running a development server using `python manage.py runserver`.\
`gregsite/settings/prod.py` contains production configuration. It's used when the application is initialized by an application server via `gregsite.wsgi` or `gregsite.asgi`.

Any settings in `local.py` override definitions in `base.py`, `dev.py` and `prod.py`.

## Development

    python manage.py migrate
    # python manage.py loaddata testdata
    # python manage.py createsuperuser
    python manage.py runserver

## API documentation

An OpenAPI schema is available at `/api/schema/`. Point your browser to `/api/schema/swagger-ui/` for an interactive Swagger UI.

Refer to [drf-spectacular](https://github.com/tfranzel/drf-spectacular/) for details about the schema generation.

## Sending notifications

    python manage.py start_notification_publisher

## Testing

    python manage.py test

## Static type analysis

Use [mypy](http://mypy-lang.org/) to run static type checks using type hints.

    mypy .

## Container images

A `Containerfile` is provided for application deployment purposes. It is not designed to be used for development.

`update-harbor-image.sh` is a utility script for building and uploading a Docker image to our private image repository `harbor.uio.no`.

It expects you to be logged in:

    docker login harbor.uio.no

## Deployment

TODO: Make a Helm chart.
